use std::f64::consts::PI;
use std::vec::Vec;

const TABLE_SIZE: usize = 32768;

pub struct Output {
    pub left: f64,
    pub right: f64,
}

pub trait Evaluatable {
    fn evaluate(&mut self) -> Output;
}

pub struct WaveGenerator {
    current_position: usize,
    lookup_table: Vec<f64>,
}

impl WaveGenerator {
    pub fn sine(frequency: f64, sample_rate: f64) -> WaveGenerator {
        let wave_table = gen_sine_wave_table();
        create_wave_generator(frequency, sample_rate, wave_table)
    }

    pub fn square(frequency: f64, sample_rate: f64) -> WaveGenerator {
        let wave_table = gen_square_wave_table();
        create_wave_generator(frequency, sample_rate, wave_table)
    }
}

impl Evaluatable for WaveGenerator {
    fn evaluate(&mut self) -> Output {
        self.current_position = 1 + self.current_position % self.lookup_table.len();
        let val = self.lookup_table[self.current_position - 1];
        Output {
            left: val,
            right: 2.0 * val,
        }
    }
}

fn create_wave_generator(frequency: f64, sample_rate: f64, wave_table: Vec<f64>) -> WaveGenerator {
    let lookup_table = gen_lookup_table(frequency, sample_rate, wave_table);
    WaveGenerator {
        current_position: 0,
        lookup_table,
    }
}

fn gen_lookup_table(frequency: f64, sample_rate: f64, wave_table: Vec<f64>) -> Vec<f64> {
    let total_samples = (sample_rate / 5.0) as usize;
    let mut lut = Vec::with_capacity(total_samples);
    let frq_tl = TABLE_SIZE as f64 / sample_rate;
    let index_increment = frq_tl * frequency;
    let mut index = 0.0;
    for _ in 0..total_samples {
        lut.push(wave_table[index as usize]);
        index += index_increment;
        if index >= TABLE_SIZE as f64 {
            index -= TABLE_SIZE as f64;
        }
    }
    lut
}

fn gen_sine_wave_table() -> Vec<f64> {
    let mut wt = Vec::with_capacity(TABLE_SIZE);
    for i in 0..TABLE_SIZE {
        wt.push((i as f64 / TABLE_SIZE as f64 * PI * 2.0).sin());
    }
    wt
}

fn gen_square_wave_table() -> Vec<f64> {
    let mut wt = Vec::with_capacity(TABLE_SIZE);
    let mut extreme = 1.0;
    for i in 0..TABLE_SIZE {
        wt.push(extreme);
        if 2 * i >= TABLE_SIZE {
            extreme *= -1.0;
        }
    }
    wt
}

#[test]
fn test_sine_evaluate() {
    let mut sine_generator = WaveGenerator::sine(440.0, 44_100.0);
    let init_phase = sine_generator.evaluate();
    assert_eq!(init_phase.left, 0.0);
    assert_eq!(init_phase.right, 0.0);
    for _ in 0..20 {
        sine_generator.evaluate();
    }
    assert!(sine_generator.evaluate().left > 0.9);

    for _ in 0..40 {
        sine_generator.evaluate();
    }
    assert!(sine_generator.evaluate().left < -0.5);
}

#[test]
fn test_square_evaluate() {
    let mut square_generator = WaveGenerator::square(440.0, 44_100.0);
    for _ in 0..100 {
        let output = square_generator.evaluate();
        assert_eq!(output.left.abs(), 1.0);
        assert_eq!(output.right.abs(), 2.0);
    }
}
