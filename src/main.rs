mod sample_generator;

#[macro_use]
extern crate clap;
extern crate portaudio;

use clap::App;
use portaudio as pa;
use sample_generator::{Evaluatable, WaveGenerator};

const CHANNELS: i32 = 2;
const NUM_SECONDS: i32 = 5;
const SAMPLE_RATE: f64 = 44_100.0;
const FRAMES_PER_BUFFER: u32 = 64;

fn main() {
    let generator = get_wave_generator();
    match run(generator) {
        Ok(_) => {}
        e => {
            eprintln!("Example failed with the following: {:?}", e);
        }
    }
}

fn get_wave_generator() -> WaveGenerator {
    let yaml = load_yaml!("../cli.yml");
    let matches = App::from_yaml(yaml).get_matches();
    let frequency = value_t!(matches, "FREQUENCY", f64).unwrap_or(440.0);
    let wave_type = matches.value_of("wave").unwrap_or("");
    match_wave_generator(wave_type, frequency)
}

fn match_wave_generator(wave_type: &str, frequency: f64) -> WaveGenerator {
    match wave_type {
        "square" => WaveGenerator::square(frequency, SAMPLE_RATE),
        "sine" | _ => WaveGenerator::sine(frequency, SAMPLE_RATE),
    }
}

fn run<T: Evaluatable + 'static>(mut generator: T) -> Result<(), pa::Error> {
    println!(
        "PortAudio Test: output sawtooth wave. SR = {}, BufSize = {}",
        SAMPLE_RATE, FRAMES_PER_BUFFER
    );

    let pa = pa::PortAudio::new()?;

    let mut settings =
        pa.default_output_stream_settings(CHANNELS, SAMPLE_RATE, FRAMES_PER_BUFFER)?;

    // we won't output out of range samples so don't bother clipping them.
    settings.flags = pa::stream_flags::CLIP_OFF;

    let callback = move |pa::OutputStreamCallbackArgs { buffer, frames, .. }| {
        let mut idx = 0;
        for _ in 0..frames {
            let phase = generator.evaluate();
            buffer[idx] = phase.left as f32;
            buffer[idx + 1] = phase.right as f32;
            idx += 2;
        }

        pa::Continue
    };

    let mut stream = pa.open_non_blocking_stream(settings, callback)?;
    stream.start()?;
    println!("Play for {} seconds.", NUM_SECONDS);
    pa.sleep(NUM_SECONDS * 1_000);

    stop_and_close(stream)?;

    println!("Test finished.");

    Ok(())
}

type Stream = pa::Stream<pa::NonBlocking, pa::Output<f32>>;

fn stop_and_close(mut stream: Stream) -> Result<(), pa::Error> {
    stream.stop()?;
    stream.close()?;
    Ok(())
}
